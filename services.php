<!DOCTYPE html>
<html>
<head>
  <title>TrackObeam</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet_index.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
</head>

<body>
<?php include("navbar.php");
?>
<div id="services" class="container-fluid">

  <h1 align="center">SERVICES</h1>
  <h2 align="center">LOCATE YOUR DESTINATION</h2>
 <h4 align="center">Track your destinations and nearby's with TrackObeam</h4>

 <p align="right"><?php include("welcome6.php"); ?></p>
 <table class="table table-bordered">
 <table class="table">
   <tbody>
      <tr>
        <td align="center"><a href="airport.php"><img src="image/Images/airport.png" /><p ><h3 align="center" style="color:#000">AIRPORT</h3></p></a></td>
        <td  align="center"><a href="ambulance.php"><img src="image/Images/ambulance.png" /><p ><h3 align="center" style="color:#000">AMBULANCE</h3></p></a></td>
        <td  align="center"><a href="apparels.php"><img src="image/Images/apparels.png" /><p ><h3 align="center" style="color:#000">APPARELS</h3></p></a></td>
        <td  align="center"><a href="atm.php"><img src="image/Images/atm.png" /><p ><h3 align="center" style="color:#000">ATM</h3></p></a></td>
        <td  align="center"><a href="automobile.php"><img src="image/Images/automobile.png" /><p ><h3 align="center" style="color:#000">AUTOMOBILE</h3></p></a></td>
        <td  align="center"><a href="banks.php"><img src="image/Images/banks.png" /><p  ><h3 align="center" style="color:#000">BANKS</h3></p></a></td>
      </tr>
    
    
      <tr>
        <td  align="center"><a href="bar.php"><img src="image/Images/bar.png" /><p ><h3 align="center" style="color:#000">BAR</h3></p></a></td>
        <td  align="center"><a href="beauty n spa.php"><img src="image/Images/beauty n spa.png" /><p ><h3 align="center" style="color:#000">>BEAUTY N SPA</h3></p></a></td>
        <td align="center"><a href="bill recharge.php"><img src="image/Images/bill recharge.png" /><p><h3 align="center" style="color:#000">BILL RECHARGE</h3></p></a></td>
        <td align="center"><a href="blood banks.php"><img src="image/Images/blood banks.png" /><p><h3 align="center" style="color:#000">BLOOD BANKS</h3></p></a></td>
        <td align="center"><a href="cafe shops.php"><img src="image/Images/cafe_shop.png" /><p><h3 align="center" style="color:#000">CAFE SHOPS</h3></p></a></td>
        <td  align="center"><a href="college.php"><img src="image/Images/college.png" /><p><h3 align="center" style="color:#000">COLLEGE</h3></p></a></td>
      </tr>
      <tr>
        <td align="center"><a href="chemist.php"><img src="image/Images/chemist.png" /><p><h3 align="center" style="color:#000">CHEMIST</h3></p></a></td>
        <td align="center"><a href="cinema.php"><img src="image/Images/cinema.png" /><p><h3 align="center" style="color:#000">CINEMA</h3></p></a></td>
        <td align="center"><a href="clinic.php"><img src="image/Images/clinic.png" /><p><h3 align="center" style="color:#000">CLINIC</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/doctor.png" /><p><h3 align="center" style="color:#000">DOCTOR</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/electronics.png" /><p><h3 align="center" style="color:#000">ELECTRONICS</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/event planner.png" /><p><h3 align="center" style="color:#000">EVENT PLANNER</h3></p></a></td>
      </tr>
      <tr>
        <td align="center"><a href="#"><img src="image/Images/gaming hubs.png" /><p><h3 align="center" style="color:#000">GAMING HUBS</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/gym.png" /><p><h3 align="center" style="color:#000">GYM</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/hospital.png" /><p><h3 align="center" style="color:#000">HOSPITALS</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/hotels.png" /><p><h3 align="center" style="color:#000">HOTELS</h3></p></a></td>
        <td align="center"><a href="#"><img src="image/Images/institution.png" /><p><h3 align="center" style="color:#000">INSTITUTIONS</h3></p></a></td>
      </tr>
    </tbody>
  </table>
 
</div>
<?php include("footer.php");?>
</body>
</html>