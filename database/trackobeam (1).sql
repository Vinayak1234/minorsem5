-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2015 at 05:02 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trackobeam`
--

-- --------------------------------------------------------

--
-- Table structure for table `atm`
--

CREATE TABLE IF NOT EXISTS `atm` (
  `bank` varchar(100) NOT NULL,
  `area` varchar(300) NOT NULL,
  `address` varchar(300) NOT NULL,
  `telno` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atm`
--

INSERT INTO `atm` (`bank`, `area`, `address`, `telno`) VALUES
('bank of india', 'noida', 'hp petrol pump,sec 15,noida', '0120-485230'),
('State Bank Of India', 'ghaziabad', 'Ghaziabad, Ghaziabad - 201010, Loni Branch', '+(91)-120-39020202'),
('State Bank Of India', 'ghaziabad', 'Vaishali Sector 4, Ghaziabad - 201010, Gaur Gravity', '1800112211,18004253800'),
('ICICI Bank Ltd', 'ghaziabad', 'Grd Flr,Commercial Chamber No-7,Nanda Bldg, Ghaziabad, Ghaziabad - 201010, Kaushambi,Aggarwal Sweets View Map', '+(91)-120-41617799'),
('ICICI Bank Ltd', 'ghaziabad', '', ''),
('ICICI Bank Ltd', 'ghaziabad', 'Shop No-4,Atlantic Plaza,Commercial P No-8, Ghaziabad, Ghaziabad - 201010, Sec-4,Vaishali Distt View Map', '+(91)-9818177799'),
('Axis Bank', 'noida', 'Plot No-5, Noida Sector 127, Noida - 201303, Logix Techno Park', '18002335577,18002095577');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE IF NOT EXISTS `hotels` (
  `hname` varchar(200) NOT NULL,
  `telno` varchar(200) NOT NULL,
  `timings` varchar(200) NOT NULL,
  `rating` float NOT NULL,
  `area` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `typeofhotel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`hname`, `telno`, `timings`, `rating`, `area`, `address`, `typeofhotel`) VALUES
('Aman hotels& Banqute', '120-4558210', '24hours', 3.3, 'ghaziabad', 'D-7,sec-16,ghaziabad', '2star'),
('park inn', '8447123007', '24hours', 4.2, 'noida', 'b-8,sec18 noida', '3star'),
('oyo rooms', '08587043850', '24hours', 4.5, 'noida', 'sec15,noida', '5star');

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `frm` varchar(50) NOT NULL,
  `too` varchar(50) NOT NULL,
  `msg` varchar(200) NOT NULL,
  `dat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`frm`, `too`, `msg`, `dat`) VALUES
('xero', 'qwerty', '    hie', '03-10-15 01-49-51 pm'),
('xero', 'xero', '    hie', '03-10-15 01-51-28 pm'),
('xero', 'xero', '    call me when you get free', '03-10-15 01-57-47 pm');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` varchar(100) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `regis`
--

CREATE TABLE IF NOT EXISTS `regis` (
  `uname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `gen` varchar(10) NOT NULL,
  `age` int(11) NOT NULL,
  `city` varchar(20) NOT NULL,
  `dat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uname`),
  KEY `uname` (`uname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regis`
--

INSERT INTO `regis` (`uname`, `name`, `pwd`, `gen`, `age`, `city`, `dat`) VALUES
('asd', 'asd', '7815696ecbf1c96e6894b779456d330e', 'male', 20, 'DELHI', '2015-10-02 09:17:16'),
('qwer', 'qwer', '514f1b439f404f86f77090fa9edc96ce', 'female', 14, 'noida', '2015-10-02 18:13:07'),
('qwerty', 'qwerty', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'male', 20, 'DELHI', '2015-10-02 17:21:51'),
('shubhcool', 'shubh', 'bc45881d835623579f2b94fc7e687c62', 'male', 20, 'DELHI', '2015-10-02 09:09:57'),
('vibhavg1', 'vibhav', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'male', 20, 'DELHI', '2015-10-02 09:16:11'),
('xero', 'aayush1', '66049c07d9e8546699fe0872fd32d8f6', 'male', 0, '', '2015-10-02 20:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `uname` varchar(100) NOT NULL,
  `rev` varchar(200) NOT NULL,
  `dat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`uname`, `rev`, `dat`) VALUES
('qwerty', '    this is a brilliant work of technollogy', '2003-10-14 21:14:15'),
('xero', 'woooooooooooooooooooooow', '2003-10-15 01:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`email`) VALUES
('aayush89j@gmail.com'),
('vibhavgoel94@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
