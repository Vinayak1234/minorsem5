<!DOCTYPE html>
<html>
<head>
  <title>TrackObeam</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet_index.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
</head>
<body>
<!-- start of footer-->
<footer class="footer-top">
	<div class="container">
				<div class="bottom-footer">
			<div class="col-md-5"> &copy; copyrights TrackObeam 2016-2017 </div>
				<div class="col-md-7">
					<ul class="footer-nav">
						<li><a href="home.php">HOME</a></li>
						<li><a href="blogs.php">BLOG</a></li>
                        <li><a href="contactus.php">CONTACT</a></li>
                        <li><a href="services.php">SERVICES</a></li>
                        <li><a href="links.php">LINKS</a></li>
					</ul>
				</div>
			<?php include("subscribe.php");?>
            </div>
	</div>
</footer>
<!-- end of footer -->
</body>
</html>