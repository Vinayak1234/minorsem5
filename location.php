<!DOCTYPE html>
<html>
<head>
  <title>TrackObeam</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet_index.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.3.0/build/cssreset/reset-min.css">
<link rel="stylesheet" type="text/css" href="css/style_map.css">

</head>

<body>


	<div class="wrapper">

		<div id="map">
			<span class="helper">Click the button below to show your location on the map</span>
			<img id="preloader" src="images/257.gif">
		</div>

		<a class="button" href="" title="">Find My Location</a>

		<div id="results">
			<span class="longitude"></span><br>
			<span class="lattitude"></span><br>
			<span class="location"></span>
		</div>

	</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>
</body>
</html>
