<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<!-- Fig. 7.16: welcome6.html    -->
<!-- Using Relational Operators -->

<html xmlns = "http://www.w3.org/1999/xhtml">
   <head> 
      <title>Using Relational Operators</title>

      <script type = "text/javascript">
         <!--
         //var name; // string entered by the user
          function dat()
{
  var d=new Date();
  var m=d.getMinutes();
  var s=d.getSeconds();
  var hours=d.getHours();
  if(hours>12)
  {
	  var h=hours-12;
	  
	  
	  var tim=h+":"+m+":"+s+  "  P.M." ;
	  }
	  else
	  {
		  var h=hours;
		  
		   var tim=h+":"+m+":"+s+  "A.M.";
		  }
	  
  
  
  document.getElementById("ti").innerHTML=tim;	
}

		  
		  
		    now = new Date(); // current date and time
			 hour = now.getHours(); 
			//setInterval(document.write(now),1000);
          //  hour = now.getHours(); // current hour (0-23)

         // read the name from the prompt box as a string
        // name = window.prompt( "Please enter your name", "GalAnt" );

         // determine whether it is morning
         if ( hour < 12 )
            document.write( "Good Morning"+"<br>" );

         // determine whether the time is PM
         if ( hour >= 12 )
         {
            // convert to a 12 hour clock
            hour = hour - 12;

            // determine whether it is before 6 PM
            if ( hour < 6 )
               document.write( "Good Afternoon"+"<br>" );

            // determine whether it is after 6 PM
            if ( hour >= 6 )
               document.write( "Good Evening"+"<br>" );
         }

         //document.writeln(" Welcome to TrackObeam!</h1>" );
         // -->
		 //var str="welcome to TRACKOBEAM!";
		// location.reload(setTimeout(myFunction, 3000));
		 //<onre="setTimeout(myFunction, 3000)">Try it</button>
		
//window.setTimeout(function(){window.close()}, 3000);

//}
      </script>

   </head>
<body onLoad="setInterval('dat()',1000)">
Time : <label id="ti"></label>
</body>
  
</html>


<!--
**************************************************************************
* (C) Copyright 1992-2004 by Deitel & Associates, Inc. and               *
* Pearson Education, Inc. All Rights Reserved.                           *
*                                                                        *
* DISCLAIMER: The authors and publisher of this book have used their     *
* best efforts in preparing the book. These efforts include the          *
* development, research, and testing of the theories and programs        *
* to determine their effectiveness. The authors and publisher make       *
* no warranty of any kind, expressed or implied, with regard to these    *
* programs or to the documentation contained in these books. The authors *
* and publisher shall not be liable in any event for incidental or       *
* consequential damages in connection with, or arising out of, the       *
* furnishing, performance, or use of these programs.                     *
**************************************************************************
-->