var map;
var markers = [];
function initialize() {
   // var latlng = new google.maps.LatLng(40.77627, -73.910965);
    var myOptions = {
        zoom: 1,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
}
var usa = new google.maps.LatLng(37.09024, -95.712891);
var brasil = new google.maps.LatLng(-14.235004, -51.92528);
var argentina = new google.maps.LatLng(-38.416097, -63.616672);
function addMarker(myloc) {
    var current;
    if (myloc == 'usa') current = usa;
    else if (myloc == 'brasil') current = brasil;
    else if (myloc == 'argentina') current = argentina;
    for (var i = 0; i < markers.length; i++)
    if (current.lat() === markers[i].position.lat() && current.lng() === markers[i].position.lng()) return;

    markers.push(new google.maps.Marker({
        map: map,
        position: current,
        title: myloc
    }));

    markers[markers.length - 1]['infowin'] = new google.maps.InfoWindow({
        content: '<div>This is a marker in ' + myloc + '</div>'
    });

    google.maps.event.addListener(markers[markers.length - 1], 'click', function() {
        this['infowin'].open(map, this);
    });
}
window.onload = initialize;